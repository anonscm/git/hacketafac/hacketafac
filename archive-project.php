<?php get_header(); ?>

<?php include('searchbar-project.php'); ?>

<section>
  <div class="container cf">
    <?php if ( have_posts() ): include('loop-project.php'); else: ?>
    <p class="empty-section"><?php _e('Nothing yet! Check back soon.','hackathon'); ?></p>
    <p class="text-center"><a href="<?php bloginfo('url'); ?>/soumettre-un-projet/" class="btn"><?php _e('Submit a project','hackathon'); ?></a></p>
		<?php endif; ?>
  </div><!-- /container -->
</section>
<?php get_footer(); ?>
