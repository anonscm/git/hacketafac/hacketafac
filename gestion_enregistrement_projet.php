<?php
$MAX_TEAM_MEMBERS = 25;
$result = '';
$thank_you = '';
if ('POST' == $_SERVER['REQUEST_METHOD'] && 
    !empty( $_POST['action'] ) && 
    $_POST['action'] == 'submit_project') {
    $post_data = array();
    foreach ($_POST as $key => $value) {
      $post_data[$key] = wp_strip_all_tags($value);
    }
    $team_members = false;
    for ($n = 0; $n < $MAX_TEAM_MEMBERS; $n++) {
      if (!empty($post_data['name_'.$n]) && !empty($post_data['email_'.$n])) {
        $team_members = true;         
      }
    }
    // Do the form validation
    if (empty($post_data['project_name'])) {
      $result = __('Please enter a name for your project.','hackathon');
    }
    elseif (!isset($post_data['agreement']) || empty($post_data['agreement'])) {
      $result = 'Merci d\'indiquer votre acceptation du traitement de vos données';
    }
    elseif (empty($post_data['description'])) {
      $result = __('Please enter a description of your project.','hackathon');
    }
    elseif (!$team_members) {
      $result = __('Please enter the name and email of at least one team member.','hackathon');
    }
    else {
      $project_name = $post_data['project_name'];
      $challenge_name = $post_data['challenge_name'];
      $challenge_id = $post_data['challenge_id'];
     // $event_id = $post_data['event_id'];
      $description = $post_data['description'];
      
	  
	  /* Création utilisateur à la volée */
	$user_name = $post_data['project_name'];
	$password = wp_generate_password();
	$user_data = array(
		'user_login' => strtolower(str_replace(' ', '', sanitize_user( $user_name, true ))),
		'user_pass' =>  $password,
		'display_name' => $user_name,
		'user_email' => $post_data['email_0'],
		'role' => 'team'
	);
	$user_id = wp_insert_user($user_data);
	if (is_wp_error($user_id)) {
		$result = '';
		foreach($user_id->errors as $error) {
			$result .= $error[0].'. ';
		}		
	} else {
		
		$user_object = get_userdata($user_id);
		$new_post = array(
			'post_title'   => $project_name,
			'post_name'    => sanitize_title_with_dashes($project_name,'','save'),
			'post_content' => $description,
			'post_status'  => 'publish',
			'post_author'  => $user_id,
			'post_type'    => 'project'
		  );
		  // Save the new post
		  $post_id = wp_insert_post($new_post, true);
		  if (is_wp_error($post_id)) {
			$contactemail = mytheme_option('contact-email');
			$result = 'Uh oh, looks like something went wrong on our end. Please email your project to <a href="mailto:'.$contactemail.'">'.$contactemail.'</a> and we will enter it for you!';
		  }
		  else {
			// Add the rest of the fields
			add_post_meta($post_id, 'challenge_name', $challenge_name);
			add_post_meta($post_id, 'challenge_id', $challenge_id);
			//add_post_meta($post_id, 'event_id', $event_id);
			add_post_meta($post_id, 'link_demo', $post_data['link_demo']);
			add_post_meta($post_id, 'link_repo', $post_data['link_repo']);
			add_post_meta($post_id, 'link_video', $post_data['link_video']);
			// Foreach team member with a name, add them to the project
			$counter = 0;
			$meta_data = array();
			for ($n = 0; $n < $MAX_TEAM_MEMBERS; $n++) {
			  if (!empty($post_data['name_'.$n])) {				  
				$meta_data[] = array(
					'name' => $post_data['name_'.$n],
					'email' => $post_data['email_'.$n],
					'role' => $post_data['role_'.$n],
					'website' => $post_data['website_'.$n]
				);
				/* Envoi du mail aux membres de l'équipe */
				$to = $post_data['email_'.$n];
				$subject = 'Vos accès à "Hacketafac"';
				$body = <<< EOT
				<p>Bonjour,</p>
				<p>Votre projet a bien enregistré sur le site "Hacketafac".</p>
				<p>Vous pourrez le modifier via l'URL : http://hacketafac.u-bordeaux.fr/wp-admin</p>
				<p>Vos accès :<br />
				- Identifiant : <strong>$user_object->user_login</strong><br />
				- Mot de passe : <strong>$password</strong>
				</p>
				
EOT;
				$headers = array('Content-Type: text/html; charset=UTF-8');
				 
				wp_mail( $to, $subject, $body, $headers );
			  } else {
				  $meta_data[] = array(
					'name' => '',
					'email' => '',
					'role' => '',
					'website' => ''
				);
			  }
			}
			update_field('field_53835f81ef6e6', $meta_data, $post_id);
			
			$thank_you = get_field('thank_you_message');
		  }
		}
	}
}
?>