<?php

/*
*  Meta box - options
*
*  This template file is used when editing a field group and creates the interface for editing options.
*
*  @type	template
*  @date	23/06/12
*/


// global
global $post;

	
// vars
$options = apply_filters('acf/field_group/get_options', array(), $post->ID);
	

?>
<table class="acf_input widefat" id="acf_options">
	<tr>
		<td class="label">
			<label for=""><?php _e("Order No.",'hackathon'); ?></label>
			<p class="description"><?php _e("Field groups are created in order <br />from lowest to highest",'hackathon'); ?></p>
		</td>
		<td>
			<?php 
			
			do_action('acf/create_field', array(
				'type'	=>	'number',
				'name'	=>	'menu_order',
				'value'	=>	$post->menu_order,
			));
			
			?>
		</td>
	</tr>
	<tr>
		<td class="label">
			<label for=""><?php _e("Position",'hackathon'); ?></label>
		</td>
		<td>
			<?php 
			
			do_action('acf/create_field', array(
				'type'	=>	'select',
				'name'	=>	'options[position]',
				'value'	=>	$options['position'],
				'choices' => array(
					'acf_after_title'	=>	__("High (after title)",'hackathon'),
					'normal'			=>	__("Normal (after content)",'hackathon'),
					'side'				=>	__("Side",'hackathon'),
				),
				'default_value' => 'normal'
			));

			?>
		</td>
	</tr>
	<tr>
		<td class="label">
			<label for="post_type"><?php _e("Style",'hackathon'); ?></label>
		</td>
		<td>
			<?php 
			
			do_action('acf/create_field', array(
				'type'	=>	'select',
				'name'	=>	'options[layout]',
				'value'	=>	$options['layout'],
				'choices' => array(
					'no_box'			=>	__("Seamless (no metabox)",'hackathon'),
					'default'			=>	__("Standard (WP metabox)",'hackathon'),
				)
			));
			
			?>
		</td>
	</tr>
	<tr id="hide-on-screen">
		<td class="label">
			<label for="post_type"><?php _e("Hide on screen",'hackathon'); ?></label>
			<p class="description"><?php _e("<b>Select</b> items to <b>hide</b> them from the edit screen",'hackathon'); ?></p>
			<p class="description"><?php _e("If multiple field groups appear on an edit screen, the first field group's options will be used. (the one with the lowest order number)",'hackathon'); ?></p>
		</td>
		<td>
			<?php 
			
			do_action('acf/create_field', array(
				'type'	=>	'checkbox',
				'name'	=>	'options[hide_on_screen]',
				'value'	=>	$options['hide_on_screen'],
				'choices' => array(
					'permalink'			=>	__("Permalink", 'hackathon'),
					'the_content'		=>	__("Content Editor",'hackathon'),
					'excerpt'			=>	__("Excerpt", 'hackathon'),
					'custom_fields'		=>	__("Custom Fields", 'hackathon'),
					'discussion'		=>	__("Discussion", 'hackathon'),
					'comments'			=>	__("Comments", 'hackathon'),
					'revisions'			=>	__("Revisions", 'hackathon'),
					'slug'				=>	__("Slug", 'hackathon'),
					'author'			=>	__("Author", 'hackathon'),
					'format'			=>	__("Format", 'hackathon'),
					'featured_image'	=>	__("Featured Image", 'hackathon'),
					'categories'		=>	__("Categories", 'hackathon'),
					'tags'				=>	__("Tags", 'hackathon'),
					'send-trackbacks'	=>	__("Send Trackbacks", 'hackathon'),
				)
			));
			
			?>
		</td>
	</tr>
</table>