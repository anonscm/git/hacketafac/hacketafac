<?php
/*
Template Name: Submit Project
*/
get_header();
include_once('gestion_enregistrement_projet.php');
?>
<div class="container cf special-page">
  <?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
  <div class="main-col center cf">
    <?php if (empty($thank_you)): ?>
    <?php the_content(); ?>
    <?php else: ?>
    <?php the_field('post_signup_paragraph'); ?>
    <?php endif; ?>
    <?php if (!empty($thank_you)): ?>
    <div class="alert alert-success">
      <?php echo $thank_you; ?>
    </div>
    <p class="text-center">
      <a href="<?php bloginfo('url'); ?>" class="btn"><?php _e('Go Back to the Home Page', 'hackathon'); ?></a>
    </p>
    <?php else: ?>
    <form id="submit_project" name="submit_project" method="post" action="<?php the_permalink(); ?>">
      <?php if (!empty($result)): ?>
      <div class="alert alert-error">
        <?php echo $result; ?>
      </div>
      <?php endif; ?>
      <div class="form-row">
        <label for="project_name"><?php _e('Project Name *','hackathon'); ?></label>
        <input type="text" name="project_name" value="<?php echo $post_data['project_name']; ?>" required />
      </div>
      <div class="form-row">
        <div class="grid grid-half">
          <label for="challenge_id" class="with-subtitle"><?php _e('Challenge','hackathon'); ?></label>
          <div class="label-subtitle"><?php _e('Select the challenge your project addresses','hackathon'); ?></div>
          <?php
            $args = array(
              'post_type' => 'challenge',
              'orderby' => 'title',
              'order' => 'ASC',
              'numberposts' => '-1'
            );
            $challenges = get_posts($args); ?>
          <select name="challenge_id">
              <option value="0"><?php _e('Select a challenge...','hackathon'); ?></option>
            <?php if ( count($challenges) > 0 ): foreach($challenges as $post): setup_postdata($post); ?>
              <option value="<?php the_id(); ?>" <?php if (isset($post_data['challenge_id']) && (get_the_id() == $post_data['challenge_id'])) echo 'selected'; ?>><?php the_title(); ?></option>
            <?php endforeach; endif; wp_reset_query(); ?>
          </select>
        </div>
        <div class="grid grid-half">
          <label for="challenge_name" class="with-subtitle"><?php _e('Other Challenge','hackathon'); ?></label>
          <div class="label-subtitle"><?php _e('If you are addressing a different challenge, describe it here','hackathon'); ?></div>
          <input type="text" name="challenge_name" value="<?php echo (isset($post_data['challenge_name'])? $post_data['challenge_name'] : ''); ?>" />
        </div>
      </div>
	<input type="hidden" name="event_id" value="0" />
     
      
      <div class="form-row">
        <label for="description"><?php _e('Description *','hackathon'); ?></label>
        <textarea name="description" required><?php echo (isset($post_data['description']) ? $post_data['description'] : ''); ?></textarea>
      </div>
      <div class="form-row">
        <div class="grid grid-half">
          <label for="link_demo"><?php _e('Link to Demo Site','hackathon'); ?></label>
          <input type="text" name="link_demo" value="<?php echo $post_data['link_demo']; ?>" />
        </div>
        <div class="grid grid-half">
          <label for="link_repo"><?php _e('Link to Code Repository','hackathon'); ?></label>
          <input type="text" name="link_repo" value="<?php echo $post_data['link_repo']; ?>" />
        </div>
      </div>
      <div class="form-row">
        <label for="link_video"><?php _e('Link to Video (YouTube or Vimeo)','hackathon'); ?></label>
        <input type="text" name="link_video" value="<?php echo $post_data['link_video']; ?>" />
      </div>
	 
      <fieldset>
      <legend><?php _e('Team Members *','hackathon'); ?><a href="#" class="btn" id="add-team-member" title="Add Team Member">+</a></legend>
      <div class="label-subtitle"><?php _e('Each Team Member\'s photo will be automatically pulled from','hackathon'); ?> <a href="http://gravatar.com" target="_blank">Gravatar</a>.</div>
        <?php for ($n = 0; $n < $MAX_TEAM_MEMBERS; $n++): ?>
        <div class="team-member-form-row" style="display: <?php 
            echo ($n == 0 || strlen($post_data['name_'.$n]) > 0) ? 'block' : 'none'; ?>">
          <img src="<?php echo 'http://www.gravatar.com/avatar/' . 
            md5(strtolower(trim($post_data['email_'.$n]))) . '?s=100&d=mm'; ?>" class="photo"/>
          <div class="content">
            <a href="#" class="close">&times;</a>
            <div class="form-row">
              <div class="grid grid-half">
                <label for="name_<?php echo $n; ?>"><?php _e('Name *','hackathon'); ?></label>
                <input type="text" name="name_<?php echo $n; ?>" value="<?php echo $post_data['name_'.$n]; ?>" 
                  class="required" />
              </div>
              <div class="grid grid-half">
                <label for="email_<?php echo $n; ?>"><?php _e('Email *','hackathon'); ?></label>
                <input type="text" class="email" name="email_<?php echo $n; ?>" value="<?php echo $post_data['email_'.$n]; ?>" />
              </div>
            </div>
            <div class="form-row">
              <div class="grid grid-half">
                <label for="role_<?php echo $n; ?>"><?php _e('Role','hackathon'); ?></label>
                <input type="text" name="role_<?php echo $n; ?>" value="<?php echo $post_data['role_'.$n]; ?>" />
              </div>
              <div class="grid grid-half">
                <label for="website_<?php echo $n; ?>"><?php _e('Website/URL','hackathon'); ?></label>
                <input type="text" name="website_<?php echo $n; ?>" value="<?php echo $post_data['website_'.$n]; ?>" />
              </div>
            </div>
          </div>
        </div>
        <?php endfor; ?>
        <div id="no-team-members">
          <p><?php _e('No one?! Really???','hackathon'); ?></p>
          <p><?php _e('At least add yourself!','hackathon'); ?></p>
        </div>
      </fieldset>
	  <div class="form-row">
        <input type="checkbox" name="agreement" value="1" aria-invalid="false" class="valid">
		<span><small>Les informations recueillies à partir de ce formulaire font l’objet d’un traitement informatique destiné à l’université de Bordeaux pour l’information des participants au concours et l'affichage sur le site web Hacketafac.
Conformément à la <a href="https://www.cnil.fr/fr/loi-78-17-du-6-janvier-1978-modifiee">loi « informatique et libertés » du 6 janvier 1978 modifiée</a>, vous disposez d’un <a target="_blank" href="https://www.cnil.fr/fr/le-droit-dacces">droit d’accès</a> et de <a href="https://www.cnil.fr/fr/le-droit-de-rectification" target="_blank">rectification</a> aux informations qui vous concernent en vous adressant à la Direction du développement : <a href="mailto:antoine.blanchard@u-bordeaux.fr">antoine.blanchard@u-bordeaux.fr</a><br />
Vous pouvez également, pour des motifs légitimes, <a href="https://www.cnil.fr/fr/le-droit-dopposition" target="_blank">vous opposer au traitement des données vous concernant</a>.</small></span>
      </div>
      <input type="hidden" name="action" value="submit_project" />
      <?php wp_nonce_field( 'submit-project' ); ?>
      <?php the_field('submit_note'); ?>
      
      <div class="form-row submit">
        <input type="submit" value="Valider" class="btn" />
      </div>
    </form>
    <?php endif; ?>
  </div><!-- /main-col -->
  <?php endwhile; ?>
  <?php endif; ?>  
</div>
<?php get_footer(); ?>
